#include <stdlib.h>
#include <stdio.h>

int main(void)
{
    const int dim_x = 1536, dim_y = 256;
    int i;
    u_int8_t t = 0;
    int position;
    const int rgb_data_size = dim_x * dim_y * 3;
    int canvas[rgb_data_size];
    u_int8_t bytebeat_data[dim_x];
    FILE *ppm_output;
    FILE *csv_output;


    /* fill canvas with white */
    for (i = 0; i < rgb_data_size; i++)
    {
        canvas[i] = 255;
    }


    /* fill the bytebeat data to plot *************************************************/
    for (i = 0; i < dim_x; i++)
    {
        bytebeat_data[i] = t;
        t++;
    }
  
  
    /* plot t *************************************************************************/
    for (i = 0; i < dim_x; i++)
    {
        position = ((dim_x * (dim_y - 1) + i) - (dim_x * bytebeat_data[i])) * 3;
        canvas[position] = 0;
        canvas[position+1] = 0;
        canvas[position+2] = 0;
    }


    /* write PPM to file */
    ppm_output = fopen("bytebeat-plot.ppm", "w");

    fprintf(ppm_output, "P3\n%d %d\n255\n", dim_x, dim_y);
    for (i = 0; i < rgb_data_size; i++)
    {
        fprintf(ppm_output, "%d ", canvas[i]);
    }
    fclose(ppm_output);


    /* write CSV to file ***************************************************************/
    csv_output = fopen("bytebeat-data.csv", "w");
    for (i = 0; i < dim_x; i++)
    {
        fprintf(csv_output, "%d,%d\n", i, bytebeat_data[i]);
    }
    fclose(csv_output);
}